<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Pet Listing</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
<!-- CONTENT HERE -->
 <h1>Pet Listing</h1>
 
 
 
 <body>

	<div>
	<?php
		session_start();
		$name = $_SESSION['name'];
		require('connect.php');
		$stmt = $mysqli->prepare("SELECT title, user FROM story");
		if(!$stmt){
			printf("Query all story list failed: %s\n", $mysqli->connect_error);
			exit;
		}
		$stmt->execute();
		$stmt->bind_result($title, $user);
		echo"<table style='width:100%'>\n";
		while($stmt->fetch()){
			echo "<tr>";
			printf("<td colspan=2><h1>%s</h1></td></tr> <tr> <td>%s</td></tr>",
				   htmlspecialchars($title),
				   htmlspecialchars($user));
			echo "<tr><td colspan=2>";
			echo "<form action = \"option.php\" method=\"POST\">";
			echo "<input type=\"hidden\" name=\"title\" value = ".htmlspecialchars($title).">";
			echo "<input type=\"submit\" name=\"read\" value=\"Read\">";
			
			if (isset($_SESSION['name']) &&
				htmlspecialchars($user) == $_SESSION['name']) {
				echo "<input type=\"submit\" name=\"edit\" value=\"edit\">";
				$_SESSION['title'] = $title;
				echo "<input type=\"submit\" name=\"delete\" value=\"delete\">";
			}
			echo "</form>";
			echo"</td></tr>";
			
		}
	?>
		<div>
		<!-- new story-->
		<?php
		//session_start();
		if (isset($_SESSION['name']) && $_SESSION['name']!== 'guest') {
			echo "<form action = \"new.php\">";
			echo "   <input type=\"submit\" value=\"Create a New Story\">";
			echo "</form>";
		} else {
			echo "<label>log in to create your story</label>";
		}
		?>
	</div>
	</div>
	<?php
	echo "you are logged in as "; echo $name;
	?>
	<p>
	<form action = "logoff.php">
	<input type = "submit" value = "Logoff">
	</form>
	</p>

</div></body>
</html>