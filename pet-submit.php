<?php
if (!$_POST['name'] || !$_POST['species'] || !$_POST['weight'] || $des = $_POST['description']){
	echo "missing information";
	header("Location: add-pet.html");
	exit;
}

$name = $_POST['name'];
$species = $_POST['species'];
$weight = $_POST['weight'];
$des = $_POST['description'];
$file = $_FILES['picture'];
echo $file;
$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'petdb');
 
if($mysqli->connect_error) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}
$stmt = $mysqli->prepare("insert into story (species, name, filename, weight, description) values (?, ?, ?, ?, ?)");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('sssss', $species,$name,$file,$weight,$des);
$stmt->execute();
$stmt->close();

if( !preg_match('/^[\w_\-]+$/', $file) ){
	echo "Invalid filename";
	exit;
}
$full_path = sprintf("/home/lduan/public_html/ica/%s", $filename);
$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($full_path);
header("Content-Type: ".$mime);
readfile($full_path);

header("pet-listing.php");
exit;
?>